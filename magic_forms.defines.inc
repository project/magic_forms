<?php
/**
 * @file
 * Magic form defines.
 */

define('MAGIC_FORMS_BASE_PATH', 'admin/config/user-interface/magic-forms');
define('MAGIC_FORMS_PERMISSION', 'administer magic forms');
define('MAGIC_FORMS_HOOK_VALIDATORS', 'magic_forms_validators');

define('MAGIC_FORMS_ERROR_SEP', '<-||->');

define('MAGIC_FORMS_VARS_DEBUGMODE', 'magic_forms_debugmode');
define('MAGIC_FORMS_VARS_DEBUGMODE_DEFAULT', FALSE);

define('MAGIC_FORMS_VARS_DEBUG_FORMID', 'magic_forms_debug_formid');
define('MAGIC_FORMS_VARS_DEBUG_FORMID_DEFAULT', FALSE);

define('MAGIC_FORMS_VARS_DEBUG_FORM', 'magic_forms_debug_form');
define('MAGIC_FORMS_VARS_DEBUG_FORM_DEFAULT', FALSE);

define('MAGIC_FORMS_VARS_APPLYTO', 'magic_forms_applyto');
define('MAGIC_FORMS_VARS_APPLYTO_DEFAULT', '');

define('MAGIC_FORMS_VARS_ERROR_HANDLING', 'magic_forms_error_handling');
define('MAGIC_FORMS_VARS_ERROR_HANDLING_DEFAULT', TRUE);
define('MAGIC_FORMS_ERROR_HANDLING', 'error-handling');

define('MAGIC_FORMS_VARS_GROUP_ERRORS', 'magic_forms_group_errors');
define('MAGIC_FORMS_VARS_GROUP_ERRORS_DEFAULT', FALSE);
define('MAGIC_FORMS_GROUP_ERRORS', 'group-errors');

define('MAGIC_FORMS_VARS_GROUP_ERRORS_ANCHOR', 'magic_forms_group_errors_anchor');
define('MAGIC_FORMS_VARS_GROUP_ERRORS_ANCHOR_DEFAULT', TRUE);
define('MAGIC_FORMS_GROUP_ERRORS_ANCHOR', 'group-errors-anchor');

define('MAGIC_FORMS_VARS_FIELD_ERRORS', 'magic_forms_field_errors');
define('MAGIC_FORMS_VARS_FIELD_ERRORS_DEFAULT', FALSE);
define('MAGIC_FORMS_FIELD_ERRORS', 'field-errors');

define('MAGIC_FORMS_VARS_FIELD_ERROR_ASPREFIX', 'magic_forms_field_error_asprefix');
define('MAGIC_FORMS_VARS_FIELD_ERROR_ASPREFIX_DEFAULT', FALSE);
define('MAGIC_FORMS_FIELD_ERROR_ASPREFIX', 'field-error-asprefix');

define('MAGIC_FORMS_VARS_FIELD_ERRORS_WRAPPER', 'magic_forms_field_errors_wrapper');
define('MAGIC_FORMS_VARS_FIELD_ERRORS_WRAPPER_DEFAULT', TRUE);
define('MAGIC_FORMS_FIELD_ERRORS_WRAPPER', 'field-errors-wrapper');

define('MAGIC_FORMS_VARS_FIELD_ERRORS_WRAPPER_CLASS', 'magic_forms_field_errors_wrapper_class');
define('MAGIC_FORMS_VARS_FIELD_ERRORS_WRAPPER_CLASS_DEFAULT', 'field-wrapper-error');
define('MAGIC_FORMS_FIELD_ERRORS_WRAPPER_CLASS', 'field-errors-wrapper-class');

define('MAGIC_FORMS_VARS_PREVENT_VALIDATION_FIELDS', 'magic_forms_prevent_validation_fields');
define('MAGIC_FORMS_VARS_PREVENT_VALIDATION_FIELDS_DEFAULT', '');
define('MAGIC_FORMS_PREVENT_VALIDATION_FIELDS', 'prevent_validation_fields');
