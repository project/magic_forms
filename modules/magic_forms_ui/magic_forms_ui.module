<?php
/**
 * @file
 * Magic forms UI module.
 */

require_once drupal_get_path('module', 'magic_forms') . '/magic_forms.defines.inc';

define('MAGIC_FORMS_UI_BASE_PATH', MAGIC_FORMS_BASE_PATH . '/ui');

/**
 * Implements hook_menu().
 */
function magic_forms_ui_menu() {
  $items = array();

  // The ui menu.
  $items[MAGIC_FORMS_UI_BASE_PATH] = array(
    'title' => 'Forms',
    'description' => 'Configure form configs.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('magic_forms_ui_list_form'),
    'access arguments' => array(MAGIC_FORMS_PERMISSION),
    'file' => 'ui/list.ui.inc',
    'type' => MENU_LOCAL_TASK,
  );

  // The list menu.
  $items[MAGIC_FORMS_UI_BASE_PATH . '/list'] = array(
    'title' => 'List',
    'description' => 'List the magic form configs.',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
  );

  // The ui menu.
  $items[MAGIC_FORMS_UI_BASE_PATH . '/add'] = array(
    'title' => 'Add',
    'description' => 'Add a new form config.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('magic_forms_ui_add_form'),
    'access arguments' => array(MAGIC_FORMS_PERMISSION),
    'file' => 'ui/add.ui.inc',
    'type' => MENU_LOCAL_ACTION,
  );

  $items[MAGIC_FORMS_UI_BASE_PATH . '/import'] = array(
    'title' => 'Import',
    'description' => 'Import a new form config.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('magic_forms_ui_import_form'),
    'access arguments' => array(MAGIC_FORMS_PERMISSION),
    'file' => 'ui/import.ui.inc',
    'type' => MENU_LOCAL_ACTION,
  );

  // The ui menu.
  $items[MAGIC_FORMS_UI_BASE_PATH . '/%/edit'] = array(
    'title callback' => 'magic_forms_ui_op_title',
    'title arguments' => array('edit', 5),
    'description' => 'Edit a form config.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('magic_forms_ui_edit_form', 5),
    'access arguments' => array(MAGIC_FORMS_PERMISSION),
    'file' => 'ui/edit.ui.inc',
  );

  // The ui menu.
  $items[MAGIC_FORMS_UI_BASE_PATH . '/%/delete'] = array(
    'title callback' => 'magic_forms_ui_op_title',
    'title arguments' => array('delete', 5),
    'description' => 'Delete a form config.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('magic_forms_ui_delete_form', 5),
    'access arguments' => array(MAGIC_FORMS_PERMISSION),
    'file' => 'ui/delete.ui.inc',
  );

  // The ui menu.
  $items[MAGIC_FORMS_BASE_PATH . '/ui/%/export'] = array(
    'title callback' => 'magic_forms_ui_op_title',
    'title arguments' => array('export', 5),
    'description' => 'Export a form config.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('magic_forms_ui_export_form', 5),
    'access arguments' => array(MAGIC_FORMS_PERMISSION),
    'file' => 'ui/export.ui.inc',
  );

  return $items;
}

/**
 * Edit config title callback.
 */
function magic_forms_ui_op_title($op, $arg) {
  return t(
    '@op config: @name',
    array(
      '@op' => ucfirst($op),
      '@name' => $arg,
    )
  );
}

/**
 * Attempt to load a form config.
 *
 * @param string $name
 *   The name of the form config.
 *
 * @return Object|bool
 *   Returns either the config object, otherwise FALSE.
 */
function magic_forms_ui_config($name) {
  $dbq = db_select('magic_forms', 'mf');
  $config = $dbq
    ->fields('mf', array())
    ->condition('mf.name', $name)
    ->execute()
    ->fetch();

  if ($config && is_string($config->data)) {
    $config->data = unserialize($config->data);
  }

  return $config;
}

/**
 * Attempt to load form configs from form id.
 *
 * @param string $form_id
 *   The forms form_id.
 *
 * @return array|bool
 *   Returns either an array of config objects, otherwise FALSE.
 */
function magic_forms_ui_config_by_form_id($form_id) {
  $dbq = db_select('magic_forms', 'mf');
  $configs = $dbq
    ->fields('mf', array())
    ->condition('mf.form_id', $form_id)
    ->execute()
    ->fetchAll();

  if ($configs && count($configs)) {
    foreach ($configs as &$config) {
      if (is_string($config->data)) {
        $config->data = unserialize($config->data);
      }
    }
  }

  return $configs;
}

/**
 * A helper function to determine if the magic form config's php evaluates.
 *
 * @param object $config
 *   The magic form config.
 * @param array $form
 *   The form.
 * @param array $form_state
 *   The form state.
 * @param string $form_id
 *   The form ID.
 *
 * @return bool
 *   Returns TRUE or FALSE.
 */
function _magic_forms_ui_magic_form_php($config, $form, $form_state, $form_id) {
  return !empty($config->data['formphp']) ? eval($config->data['formphp']) : TRUE;
}

/**
 * Helper function to apply field configuration.
 */
function _magic_forms_ui_magic_form_element(&$element, $field_config) {
  if (isset($field_config['title'])) {
    $element['#title'] = $field_config['title'] ? $field_config['title'] : NULL;
  }

  if (isset($field_config['description'])) {
    $element['#description'] = $field_config['description'] ? $field_config['description'] : NULL;
  }

  $element['#magic-forms'] = $field_config;
}

/**
 * Helper function to apply the magic form configuration to the form.
 *
 * @param object $config
 *   The magic form config.
 * @param array $form
 *   The form.
 * @param array $form_state
 *   The form state.
 * @param string $form_id
 *   The form ID.
 */
function magic_forms_ui_magic_form($config, &$form, &$form_state, $form_id) {
  $form['#magic-forms'] = $config->data['config'];

  if (isset($config->data['fields']) && count($config->data['fields'])) {
    foreach ($config->data['fields'] as $xpath => $field_config) {
      _magic_forms_get_form_element(
        $form,
        $xpath,
        '_magic_forms_ui_magic_form_element',
        array($field_config)
      );
    }
  }
}

/**
 * Implements hook_magic_forms_applt_to().
 */
function magic_forms_ui_magic_forms_apply_to(&$apply_to, &$form, &$form_state, $form_id) {
  $configs = magic_forms_ui_config_by_form_id($form_id);
  if ($configs && count($configs)) {
    foreach ($configs as $config) {
      if ($config->status && _magic_forms_ui_magic_form_php($config, $form, $form_state, $form_id)) {
        magic_forms_ui_magic_form($config, $form, $form_state, $form_id);
        break;
      }
    }
  }
}
